package client

import (
	"bytes"
	"github.com/pkg/errors"
	"github.com/valyala/fasthttp"
	"gitlab.com/adsys1/staticd/app"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Client struct {
	*http.Client

	static  *url.URL
	manager *url.URL
	token   string
}

// for testing
func (c *Client) GetAsset(path string) (*app.Asset, error) {
	uri := c.static.String() + path

	req, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	asset := &app.Asset{
		Path:         path,
		Length:       len(b),
		LengthGzip:   0,
		ETag:         []byte(resp.Header.Get(fasthttp.HeaderETag)),
		LastModified: []byte(resp.Header.Get(fasthttp.HeaderLastModified)),
		ContentType:  []byte(resp.Header.Get(fasthttp.HeaderContentType)),
		Data:         b,
		Gzipped:      nil,
	}

	return asset, nil
}

// upload asset
func (c *Client) SetAsset(asset app.Asset) error {
	uri := c.manager.String() + asset.Path

	req, err := http.NewRequest(http.MethodPost, uri, bytes.NewBuffer(asset.Data))
	if err != nil {
		return err
	}

	req.Header.Add("X-Auth-Token", c.token)

	resp, err := c.Do(req)
	if err != nil {
		return err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusAccepted {
		return errors.Errorf("server returns code %d", resp.StatusCode)
	}

	return nil
}
