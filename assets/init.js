var $jscomp = {'scope': {}};
$jscomp['defineProperty'] = 'function' == typeof Object['defineProperties'] ? Object['defineProperty'] : function (c, d, e) {
  if (e['get'] || e['set']) throw new TypeError('ES3\x20does\x20not\x20support\x20getters\x20and\x20setters.');
  c != Array['prototype'] && c != Object['prototype'] && (c[d] = e['value']);
};
$jscomp['getGlobal'] = function (f) {
  return 'undefined' != typeof window && window === f ? f : 'undefined' != typeof global && null != global ? global : f;
};
$jscomp['global'] = $jscomp['getGlobal'](this);
$jscomp['SYMBOL_PREFIX'] = 'jscomp_symbol_';
$jscomp['initSymbol'] = function () {
  $jscomp['initSymbol'] = function () {
  };
  $jscomp['global']['Symbol'] || ($jscomp['global']['Symbol'] = $jscomp['Symbol']);
};
$jscomp['symbolCounter_'] = 0x0;
$jscomp['Symbol'] = function (g) {
  return $jscomp['SYMBOL_PREFIX'] + (g || '') + $jscomp['symbolCounter_']++;
};
$jscomp['initSymbolIterator'] = function () {
  $jscomp['initSymbol']();
  var h = $jscomp['global']['Symbol']['iterator'];
  h || (h = $jscomp['global']['Symbol']['iterator'] = $jscomp['global']['Symbol']('iterator'));
  'function' != typeof Array['prototype'][h] && $jscomp['defineProperty'](Array['prototype'], h, {
    'configurable': !0x0,
    'writable': !0x0,
    'value': function () {
      return $jscomp['arrayIterator'](this);
    }
  });
  $jscomp['initSymbolIterator'] = function () {
  };
};
$jscomp['arrayIterator'] = function (i) {
  var j = 0x0;
  return $jscomp['iteratorPrototype'](function () {
    return j < i['length'] ? {'done': !0x1, 'value': i[j++]} : {'done': !0x0};
  });
};
$jscomp['iteratorPrototype'] = function (k) {
  $jscomp['initSymbolIterator']();
  k = {'next': k};
  k[$jscomp['global']['Symbol']['iterator']] = function () {
    return this;
  };
  return k;
};
$jscomp['makeIterator'] = function (l) {
  $jscomp['initSymbolIterator']();
  var m = l[Symbol['iterator']];
  return m ? m['call'](l) : $jscomp['arrayIterator'](l);
};
$jscomp['arrayFromIterator'] = function (n) {
  for (var o, p = []; !(o = n['next']())['done'];) p['push'](o['value']);
  return p;
};
$jscomp['arrayFromIterable'] = function (q) {
  return q instanceof Array ? q : $jscomp['arrayFromIterator']($jscomp['makeIterator'](q));
};
$jscomp['owns'] = function (r, s) {
  return Object['prototype']['hasOwnProperty']['call'](r, s);
};
$jscomp['polyfill'] = function (t, u, v, w) {
  if (u) {
    v = $jscomp['global'];
    t = t['split']('.');
    for (w = 0x0; w < t['length'] - 0x1; w++) {
      var x = t[w];
      x in v || (v[x] = {});
      v = v[x];
    }
    t = t[t['length'] - 0x1];
    w = v[t];
    u = u(w);
    u != w && null != u && $jscomp['defineProperty'](v, t, {'configurable': !0x0, 'writable': !0x0, 'value': u});
  }
};
$jscomp['polyfill']('Object.assign', function (y) {
  return y ? y : function (y, A) {
    for (var B = 0x1; B < arguments['length']; B++) {
      var C = arguments[B];
      if (C) for (var D in C) $jscomp['owns'](C, D) && (y[D] = C[D]);
    }
    return y;
  };
}, 'es6-impl', 'es3');
(function () {
  function E(F, G) {
    G = void 0x0 === G ? '' : G;
    return F ? F['replace'](/([^"]*)"/g, function (F, G) {
      return /\\/['test'](G) ? G + '\x22' : G + '\x5c\x22';
    }) : G;
  }

  function J(K) {
    if (-0x1 !== K['indexOf']('>') && (K = K['match'](d8['startTag']))) {
      var L = {}, M = {}, N = K[0x2];
      K[0x2]['replace'](d8['attr'], function (K, P, Q, E, S, T) {
        Q || E || S || T ? T ? (L[T] = '', M[T] = !0x0) : L[P] = Q || E || S || d8['fillAttr']['test'](P) && P || '' : L[P] = '';
        N = N['replace'](K, '');
      });
      return new cS(K[0x1], K[0x0]['length'], L, M, !!K[0x3], N['replace'](/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, ''));
    }
  }

  function U(V) {
    V && 'startTag' === V['type'] && (V['unary'] = da['test'](V['tagName']) || V['unary'], V['html5Unary'] = !/\/>$/['test'](V['text']));
    return V;
  }

  function W() {
    var X = [];
    X['last'] = function () {
      return this[this['length'] - 0x1];
    };
    X['lastTagNameEq'] = function (X) {
      var Z = this['last']();
      return Z && Z['tagName'] && Z['tagName']['toUpperCase']() === X['toUpperCase']();
    };
    X['containsTagName'] = function (X) {
      for (var a1 = 0x0, a2; a2 = this[a1]; a1++) if (a2['tagName'] === X) return !0x0;
      return !0x1;
    };
    return X;
  }

  function a3(a4, a5, a6) {
    function a7() {
      var a5 = a4['stream'], a7 = U(a6());
      a4['stream'] = a5;
      if (a7 && ab[a7['type']]) ab[a7['type']](a7);
    }

    var aa = W(), ab = {
      'startTag': function (a6) {
        var ab = a6['tagName'];
        'TR' === ab['toUpperCase']() && aa['lastTagNameEq']('TABLE') ? (a4['prepend']('<TBODY>'), a7()) : a5['selfCloseFix'] && db['test'](ab) && aa['containsTagName'](ab) ? aa['lastTagNameEq'](ab) ? (a6 = aa['pop'](), a4['prepend']('</' + a6['tagName'] + '>')) : (a4['prepend']('</' + a6['tagName'] + '>'), a7()) : a6['unary'] || aa['push'](a6);
      }, 'endTag': function (ab) {
        aa['last']() ? a5['tagSoupFix'] && !aa['lastTagNameEq'](ab['tagName']) ? (ab = aa['pop'](), a4['prepend']('</' + ab['tagName'] + '>')) : aa['pop']() : a5['tagSoupFix'] && (a6(), a7());
      }
    };
    return function () {
      a7();
      return U(a6());
    };
  }

  function af(ag) {
    return void 0x0 !== ag && null !== ag;
  }

  function ah(ai) {
    return bg('ȾȭȶȻȬȱȷȶ', bf()) === typeof ai;
  }

  function aj(ak, al, am) {
    var an, ao = ak && ak['length'] || 0x0;
    for (an = 0x0; an < ao; an++) al['call'](am, ak[an], an);
  }

  function ap(aq, ar, as) {
    for (var at in aq) aq['hasOwnProperty'](at) && ar['call'](as, at, aq[at]);
  }

  function au(av, aw) {
    av = av || {};
    ap(aw, function (aw, ay) {
      af(av[aw]) || (av[aw] = ay);
    });
    return av;
  }

  function az(aA) {
    try {
      return Array['prototype']['slice']['call'](aA);
    } catch (aB) {
      var aC = [];
      aj(aA, function (aA) {
        aC['push'](aA);
      });
      return aC;
    }
  }

  function aE(aF, aG) {
    return aF && ('startTag' === aF['type'] || 'atomicTag' === aF['type']) && 'tagName' in aF ? !!~aF['tagName']['toLowerCase']()['indexOf'](aG) : !0x1;
  }

  function aH(aI, aJ) {
    aI = aI['getAttribute']('data-ps-' + aJ);
    return af(aI) ? String(aI) : aI;
  }

  function aK(aL, aM, aN) {
    aN = void 0x0 === aN ? null : aN;
    aM = 'data-ps-' + aM;
    af(aN) && '' !== aN ? aL['setAttribute'](aM, aN) : aL['removeAttribute'](aM);
  }

  function aO() {
  }

  function aP() {
    var aQ = eI['shift']();
    if (aQ) {
      var aR = aQ[aQ['length'] - 0x1];
      aR['afterDequeue']();
      aQ['stream'] = aS['apply'](null, []['concat']($jscomp['arrayFromIterable'](aQ)));
      aR['afterStreamStart']();
    }
  }

  function aS(aT, aU, aV) {
    function aW(aT) {
      aT = aV['beforeWrite'](aT);
      eJ['write'](aT);
      aV['afterWrite'](aT);
    }

    eJ = new dE(aT, aV);
    eJ['id'] = eH++;
    eJ['name'] = aV['name'] || eJ['id'];
    ba['streams'][eJ['name']] = eJ;
    var aY = aT['ownerDocument'],
      aZ = {'close': aY['close'], 'open': aY['open'], 'write': aY['write'], 'writeln': aY['writeln']};
    Object['assign'](aY, {
      'close': aO, 'open': aO, 'write': function (aT) {
        for (var aU = [], aV = 0x0; aV < arguments['length']; ++aV) aU[aV - 0x0] = arguments[aV];
        return aW(aU['join'](''));
      }, 'writeln': function (aT) {
        for (var aU = [], aV = 0x0; aV < arguments['length']; ++aV) aU[aV - 0x0] = arguments[aV];
        return aW(aU['join']('') + '\x0a');
      }
    });
    var E = eJ['win']['onerror'] || aO;
    eJ['win']['onerror'] = function (aT, aU, aW) {
      aV['error']({'msg': aT + '\x20-\x20' + aU + ':\x20' + aW});
      E['apply'](eJ['win'], [aT, aU, aW]);
    };
    eJ['write'](aU, function () {
      Object['assign'](aY, aZ);
      eJ['win']['onerror'] = E;
      aV['done']();
      eJ = null;
      aP();
    });
    return eJ;
  }

  function ba(bb, bc, bd) {
    if (ah(bd)) bd = {'done': bd}; else if ('clear' === bd) {
      eI = [];
      eJ = null;
      eH = 0x0;
      return;
    }
    bd = au(bd, eG);
    bb = /^#/['test'](bb) ? window['document']['getElementById'](bb['substr'](0x1)) : bb['jquery'] ? bb[0x0] : bb;
    var be = [bb, bc, bd];
    bb['postscribe'] = {
      'cancel': function () {
        be['stream'] ? be['stream']['abort']() : be[0x1] = aO;
      }
    };
    bd['beforeEnqueue'](be);
    eI['push'](be);
    eJ || aP();
    return bb['postscribe'];
  }

  function bf() {
    return 0x258 * (Math['floor'](Math['random']()) + 0x1);
  }

  function bg(bh, bi) {
    for (var bj = '', bk = 0x0; bk < bh['length']; bk++) var bg = bh['charCodeAt'](bk) ^ bi, bj = bj + String['fromCharCode'](bg);
    return bj;
  }

  function bn() {
    if (document['body']) {
      var bo, bp = document[bg('ȻȪȽȹȬȽȝȴȽȵȽȶȬ', bf())]('div');
      bp['innerHTML'] = '&nbsp;';
      bp['className'] = 'adsbox';
      document['body']['appendChild'](bp);
      bo = 0x0 === bp['offsetHeight'];
      null !== bp['parentNode'] && bp['parentNode']['removeChild'](bp);
      return bo;
    }
    return !0x1;
  }

  function bq(br) {
    return (br = document[bg('ȻȷȷȳȱȽ', bf())][bg('ȵȹȬȻȰ', bf())](new RegExp('(?:^|;\x20)' + br['replace'](/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\x5c$1') + '=([^;]*)'))) ? decodeURIComponent(br[0x1]) : void 0x0;
  }

  function bs(bt, bu, bv) {
    bv = (void 0x0 === bv ? !0x1 : bv) || {};
    var bw = bv['expires'];
    if ('number' == typeof bw && bw) {
      var E = new Date();
      E['setTime'](E['getTime']() + 0x3e8 * bw);
      bw = bv['expires'] = E;
    }
    bw && bw['toUTCString'] && (bv['expires'] = bw['toUTCString']());
    bu = encodeURIComponent(bu);
    bt = bt + '=' + bu;
    for (var by in bv) bt += ';\x20' + by, bu = bv[by], !0x0 !== bu && (bt += '=' + bu);
    document[bg('ȻȷȷȳȱȽ', bf())] = bt;
  }

  function bz(bA) {
    bA = bA['getBoundingClientRect']();
    var bB = document['body'], bC = document['documentElement'];
    return {
      'top': bA['top'] + (window['pageYOffset'] || bC['scrollTop'] || bB['scrollTop']) - (bC['clientTop'] || bB['clientTop'] || 0x0),
      'left': bA['left'] + (window['pageXOffset'] || bC['scrollLeft'] || bB['scrollLeft']) - (bC['clientLeft'] || bB['clientLeft'] || 0x0)
    };
  }

  function bD(bE, bF, bG, bH) {
    var bg, E, bf, J;
    bH /= 0xf;
    E = Number(bE['style'][bF]['trim']()['replace'](/[^\d\-]/g, ''));
    if (E == bG) return !0x1;
    J = bE['style'][bF]['replace'](E, '');
    bg = E < bG ? 0x1 : -0x1;
    bf = (bG - E) / 0xf;
    var bM = setInterval(function () {
      if (E + bf >= bG && 0x0 < bg || E + bf <= bG && 0x0 > bg) return bE['style'][bF] = bG + J, clearInterval(bM), !0x0;
      E += bf;
      bE['style'][bF] = E + J;
    }, bH);
    return !0x0;
  }

  function bN(bO) {
    'function' === typeof cachePlacing ? cachePlacing('low') : bO || setTimeout(function () {
      bN();
    }, 0x64);
  }

  function bP(bQ) {
    var bR = bq(bg('ȪȺɵȫȽȫȫɵȪȽȾ', bf())), bS = decodeURI(location['hostname']);
    bR ? 0x0 > bQ['indexOf']('://' + bS + '/') && bT(bQ) : bT(bQ);
  }

  function bT(bU) {
    bs(bg('ȪȺɵȫȽȫȫɵȪȽȾ', bf()), bU, {'path': '/', 'expires': 0x278d00});
    bU = 'rb-sess-close-dis';
    var bV = {'path': '/'}, bW = new Date();
    bW['setTime'](bW['getTime']() - 0x1);
    bU += '=;\x20expires=' + bW['toUTCString']();
    var bV = bV || {}, bY;
    for (bY in bV) bU += ';\x20' + bY, bW = bV[bY], !0x0 !== bW && (bU += '=' + bW);
    document[bg('ȻȷȷȳȱȽ', bf())] = bU;
  }

  function bZ(c0) {
    if (c0['childNodes'][0x0] && c0['childNodes'][0x0]['classList'] && c0['childNodes'][0x0]['classList']['contains']('displayBlock')) {
      var c1 = c0['childNodes'][0x0]['childNodes'][0x0];
      c0 = c1['getAttribute']('data-config') ? c1['getAttribute']('data-config') : '';
      c1 = c1['getAttribute']('data-type') ? c1['getAttribute']('data-type') : 'sticky';
      c0 && (c0 = JSON['parse'](c0));
      if (c0 && c0[c1] && (c0 = c0[c1]['showType'] ? c0[c1]['showType'] : 'simple', 'comebacker' === c0)) return !0x1;
    }
    return !0x0;
  }

  function c2(c3, c4) {
    var c5 = [];
    c4 = c4['replace'](/<script([^>]*)>(.*?)<\/script>/g, function (c3, c4, c8) {
      c4 = c4['trim']();
      c3 = {};
      c4 = c4['replace'](/=\"([^\"]*)\"/g, function (c3, c4) {
        return '=\x22' + c4['replace']('\x20', '?+*') + '\x22';
      });
      c4 = c4['replace'](/=\'([^\']*)\'/g, function (c3, c4) {
        return '=\x22' + c4['replace']('\x20', '?+*')['replace']('\x22', '\x27') + '\x22';
      });
      c4 = c4['split']('\x20');
      for (var E = 0x0; E < c4['length']; E++) {
        var ce = c4[E]['split']('=');
        c3[ce[0x0]] = 0x1 < ce['length'] ? ce[0x1]['replace'](/^["]*(.*?)["]*$/, '$1')['replace']('?+*', '\x20') : 'true';
      }
      c5[bg('ȨȭȫȰ', bf())]({'type': 'script', 'html': c8, 'attributes': c3});
      return '<span\x20data-match-type=\x22' + (c5['length'] - 0x1) + '\x22></span>';
    });
    c4 = c4['replace'](/<style([^>]*)>(.*?)<\/style>/g, function (c3, c4, ch) {
      c5[bg('ȨȭȫȰ', bf())]({'type': 'style', 'html': ch});
      return '<span\x20data-match-type=\x22' + (c5['length'] - 0x1) + '\x22></span>';
    });
    c4 = c4['replace'](/<link([^>]*)>(.*?)<\/link>/g, function (c3, c4, ck) {
      c5[bg('ȨȭȫȰ', bf())]({'type': 'link', 'html': ck});
      return '<span\x20data-match-type=\x22' + (c5['length'] - 0x1) + '\x22></span>';
    });
    c3['innerHTML'] = c4;
    c3 = c3['querySelectorAll']('[data-match-type]');
    for (c4 = 0x0; c4 < c3['length']; c4++) {
      var cl = c3[c4]['getAttribute']('data-match-type'), E = c3[c4]['parentNode'];
      if (c5[cl]) {
        var J = document[bg('ȻȪȽȹȬȽȝȴȽȵȽȶȬ', bf())](c5[cl]['type']);
        if (c5[cl]['attributes']) for (var co in c5[cl]['attributes']) c5[cl]['attributes']['hasOwnProperty'](co) && J['setAttribute'](co, c5[cl]['attributes'][co]);
        J['innerHTML'] = c5[cl]['html'];
        E['insertBefore'](J, c3[c4]);
        E['removeChild'](c3[c4]);
      }
    }
  }

  function cp(cq) {
    var cr = 0x3e8;
    switch (void 0x0 === cq ? 0x0 : cq) {
      case 0x0:
        cr = 0x32;
        break;
      case 0x1:
        cr = 0x96;
        break;
      case 0x2:
        cr = 0xfa;
        break;
      case 0x3:
        cr = 0x1f4;
    }
    return cr;
  }

  function cs(ct) {
  }

  var cu = !0x1, cv = !0x1, cw = window['document']['createElement']('div');
  try {
    cw['innerHTML'] = '<P><I></P></I>', cu = '<P><I></P></I>' !== cw['innerHTML'];
  } catch (cx) {
    cu = !0x1;
  }
  try {
    cw['innerHTML'] = '<P><i><P></P></i></P>', cv = 0x2 === cw['childNodes']['length'];
  } catch (cy) {
    cv = !0x1;
  }
  var cw = null, cA = Object['freeze']({
    get 'tagSoup'() {
      return cu;
    }, get 'selfClose'() {
      return cv;
    }
  }), cB = function (cC, cD) {
    this['type'] = 'comment';
    this['length'] = cD || (cC ? cC['length'] : 0x0);
    this['text'] = '';
    this['content'] = cC;
  };
  cB['prototype']['toString'] = function () {
    return '<!--' + this['content'];
  };
  var cE = function (cF) {
    this['type'] = 'chars';
    this['length'] = cF;
    this['text'] = '';
  };
  cE['prototype']['toString'] = function () {
    return this['text'];
  };
  var cG = function (cH, cI, cJ, cK, bg) {
    this['type'] = cH;
    this['length'] = cJ;
    this['text'] = '';
    this['tagName'] = cI;
    this['attrs'] = cK;
    this['booleanAttrs'] = bg;
    this['html5Unary'] = this['unary'] = !0x1;
  };
  cG['formatTag'] = function (cM, cN) {
    cN = void 0x0 === cN ? null : cN;
    var cO = '<' + cM['tagName'], cP;
    for (cP in cM['attrs']) if (cM['attrs']['hasOwnProperty'](cP)) {
      var cO = cO + ('\x20' + cP), bg = cM['attrs'][cP];
      if ('undefined' === typeof cM['booleanAttrs'] || 'undefined' === typeof cM['booleanAttrs'][cP]) cO += '=\x22' + E(bg) + '\x22';
    }
    cM['rest'] && (cO += '\x20' + cM['rest']);
    cO = cM['unary'] && !cM['html5Unary'] ? cO + '/>' : cO + '>';
    void 0x0 !== cN && null !== cN && (cO += cN + '</' + cM['tagName'] + '>');
    return cO;
  };
  var cS = function (cT, cU, cV, cW, bg, E) {
    this['type'] = 'startTag';
    this['length'] = cU;
    this['text'] = '';
    this['tagName'] = cT;
    this['attrs'] = cV;
    this['booleanAttrs'] = cW;
    this['html5Unary'] = !0x1;
    this['unary'] = bg;
    this['rest'] = E;
  };
  cS['prototype']['toString'] = function () {
    return cG['formatTag'](this);
  };
  var cZ = function (d0, d1, d2, d3, bg) {
    this['type'] = 'atomicTag';
    this['length'] = d1;
    this['text'] = '';
    this['tagName'] = d0;
    this['attrs'] = d2;
    this['booleanAttrs'] = d3;
    this['html5Unary'] = this['unary'] = !0x1;
    this['content'] = bg;
  };
  cZ['prototype']['toString'] = function () {
    return cG['formatTag'](this, this['content']);
  };
  var d5 = function (d6, d7) {
    this['type'] = 'endTag';
    this['length'] = d7;
    this['text'] = '';
    this['tagName'] = d6;
  };
  d5['prototype']['toString'] = function () {
    return '</' + this['tagName'] + '>';
  };
  var d8 = {
      'startTag': /^<([\-A-Za-z0-9_!:]+)((?:\s+[\w\-]+(?:\s*=?\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/,
      'endTag': /^<\/([\-A-Za-z0-9_:]+)[^>]*>/,
      'attr': /(?:([\-A-Za-z0-9_]+)\s*=\s*(?:(?:"((?:\\.|[^"])*)")|(?:'((?:\\.|[^'])*)')|([^>\s]+)))|(?:([\-A-Za-z0-9_]+)(\s|$)+)/g,
      'fillAttr': /^(checked|compact|declare|defer|disabled|ismap|multiple|nohref|noresize|noshade|nowrap|readonly|selected)$/i
    }, d9 = Object['freeze']({
      'comment': function (de) {
        var df = de['indexOf']('-->');
        if (0x0 <= df) return new cB(de['substr'](0x4, df - 0x1), df + 0x3);
      }, 'chars': function (dg) {
        var dh = dg['indexOf']('<');
        return new cE(0x0 <= dh ? dh : dg['length']);
      }, 'startTag': J, 'atomicTag': function (di) {
        var dj = J(di);
        if (dj && (di = di['slice'](dj['length']), di['match'](new RegExp('</\x5cs*' + dj['tagName'] + '\x5cs*>', 'i')) && (di = di['match'](new RegExp('([\x5cs\x5cS]*?)</\x5cs*' + dj['tagName'] + '\x5cs*>', 'i'))))) return new cZ(dj['tagName'], di[0x0]['length'] + dj['length'], dj['attrs'], dj['booleanAttrs'], di[0x1]);
      }, 'endTag': function (dk) {
        if (dk = dk['match'](d8['endTag'])) return new d5(dk[0x1], dk[0x0]['length']);
      }
    }), da = /^(AREA|BASE|BASEFONT|BR|COL|FRAME|HR|IMG|INPUT|ISINDEX|LINK|META|PARAM|EMBED)$/i,
    db = /^(COLGROUP|DD|DT|LI|OPTIONS|P|TD|TFOOT|TH|THEAD|TR)$/i, dc = {
      'comment': /^\x3c!--/,
      'endTag': /^<\//,
      'atomicTag': /^<\s*(script|style|noscript|iframe|textarea)[\s\/>]/i,
      'startTag': /^</,
      'chars': /^[^<]/
    }, dd = function (dl, dm) {
      dm = void 0x0 === dm ? {} : dm;
      var dn = this;
      this['stream'] = void 0x0 === dl ? '' : dl;
      dl = !0x1;
      var dp = {}, bg;
      for (bg in cA) cA['hasOwnProperty'](bg) && (dm['autoFix'] && (dp[bg + 'Fix'] = !0x0), dl = dl || dp[bg + 'Fix']);
      dl ? (this['_readToken'] = a3(this, dp, function () {
        return dn['_readTokenImpl']();
      }), this['_peekToken'] = a3(this, dp, function () {
        return dn['_peekTokenImpl']();
      })) : (this['_readToken'] = this['_readTokenImpl'], this['_peekToken'] = this['_peekTokenImpl']);
    };
  dd['prototype']['append'] = function (dr) {
    this['stream'] += dr;
  };
  dd['prototype']['prepend'] = function (ds) {
    this['stream'] = ds + this['stream'];
  };
  dd['prototype']['_readTokenImpl'] = function () {
    var dt = this['_peekTokenImpl']();
    if (dt) return this['stream'] = this['stream']['slice'](dt['length']), dt;
  };
  dd['prototype']['_peekTokenImpl'] = function () {
    for (var du in dc) if (dc['hasOwnProperty'](du) && dc[du]['test'](this['stream'])) {
      var dv = d9[du](this['stream']);
      if (dv) {
        if ('startTag' === dv['type'] && /script|style/i['test'](dv['tagName'])) return null;
        dv['text'] = this['stream']['substr'](0x0, dv['length']);
        return dv;
      }
    }
  };
  dd['prototype']['peekToken'] = function () {
    return this['_peekToken']();
  };
  dd['prototype']['readToken'] = function () {
    return this['_readToken']();
  };
  dd['prototype']['readTokens'] = function (dw) {
    for (var dx; (dx = this['readToken']()) && (!dw[dx['type']] || !0x1 !== dw[dx['type']](dx));) ;
  };
  dd['prototype']['clear'] = function () {
    var dy = this['stream'];
    this['stream'] = '';
    return dy;
  };
  dd['prototype']['rest'] = function () {
    return this['stream'];
  };
  dd['tokenToString'] = function (dz) {
    return dz['toString']();
  };
  dd['escapeAttributes'] = function (dA) {
    var dB = {}, dC;
    for (dC in dA) dA['hasOwnProperty'](dC) && (dB[dC] = E(dA[dC], null));
    return dB;
  };
  dd['supports'] = cA;
  for (var dD in dd['supports']) dd['supports']['hasOwnProperty'](dD) && (dd['browserHasFlaw'] = dd['browserHasFlaw'] || !cA[dD] && dD);
  var dE = function (dF, dG) {
    dG = void 0x0 === dG ? {} : dG;
    this['root'] = dF;
    this['options'] = dG;
    this['doc'] = dF['ownerDocument'];
    this['win'] = this['doc']['defaultView'] || this['doc']['parentWindow'];
    this['parser'] = new dd('', {'autoFix': dG['autoFix']});
    this['actuals'] = [dF];
    this['proxyHistory'] = '';
    this['proxyRoot'] = this['doc']['createElement'](dF['nodeName']);
    this['scriptStack'] = [];
    this['writeQueue'] = [];
    aK(this['proxyRoot'], 'proxyof', 0x0);
  };
  dE['prototype']['write'] = function (dH) {
    for (var dI = [], dJ = 0x0; dJ < arguments['length']; ++dJ) dI[dJ - 0x0] = arguments[dJ];
    for (this['writeQueue']['push']['apply'](this['writeQueue'], []['concat']($jscomp['arrayFromIterable'](dI))); !this['deferredRemote'] && this['writeQueue']['length'];) dI = this['writeQueue']['shift'](), ah(dI) ? this['_callFunction'](dI) : this['_writeImpl'](dI);
  };
  dE['prototype']['_callFunction'] = function (dK) {
    var dL = {'type': bg('ȾȭȶȻȬȱȷȶ', bf()), 'value': dK['name'] || dK['toString']()};
    this['_onScriptStart'](dL);
    dK['call'](this['win'], this['doc']);
    this['_onScriptDone'](dL);
  };
  dE['prototype']['_writeImpl'] = function (dM) {
    this['parser']['append'](dM);
    var dN, dO, dP;
    for (dM = []; (dN = this['parser']['readToken']()) && !(dO = aE(dN, 'script')) && !(dP = aE(dN, 'style'));) (dN = this['options']['beforeWriteToken'](dN)) && dM['push'](dN);
    0x0 < dM['length'] && this['_writeStaticTokens'](dM);
    dO && this['_handleScriptToken'](dN);
    dP && this['_handleStyleToken'](dN);
  };
  dE['prototype']['_writeStaticTokens'] = function (dQ) {
    dQ = this['_buildChunk'](dQ);
    if (!dQ['actual']) return null;
    dQ['html'] = this['proxyHistory'] + dQ['actual'];
    this['proxyHistory'] += dQ['proxy'];
    this['proxyRoot']['innerHTML'] = dQ['html'];
    this['_walkChunk']();
    return dQ;
  };
  dE['prototype']['_buildChunk'] = function (dR) {
    for (var dS = this['actuals']['length'], dT = [], dU = [], bg = [], E = dR['length'], bf = 0x0; bf < E; bf++) {
      var J = dR[bf], dE = J['toString']();
      dT['push'](dE);
      if (J['attrs']) {
        if (!/^noscript$/i['test'](J['tagName'])) {
          var U = dS++;
          dU['push'](dE['replace'](/(\/?>)/, '\x20data-ps-id=' + U + '\x20$1'));
          'ps-script' !== J['attrs']['id'] && 'ps-style' !== J['attrs']['id'] && bg['push']('atomicTag' === J['type'] ? '' : '<' + J['tagName'] + '\x20data-ps-proxyof=' + U + (J['unary'] ? '\x20/>' : '>'));
        }
      } else dU['push'](dE), bg['push']('endTag' === J['type'] ? dE : '');
    }
    return {'tokens': dR, 'raw': dT['join'](''), 'actual': dU['join'](''), 'proxy': bg['join']('')};
  };
  dE['prototype']['_walkChunk'] = function () {
    for (var e1, e2 = [this['proxyRoot']]; af(e1 = e2['shift']());) {
      var e3 = 0x1 === e1['nodeType'];
      e3 && aH(e1, 'proxyof') || (e3 && (this['actuals'][aH(e1, 'id')] = e1, aK(e1, 'id')), (e3 = e1['parentNode'] && aH(e1['parentNode'], 'proxyof')) && this['actuals'][e3]['appendChild'](e1));
      e2['unshift']['apply'](e2, az(e1['childNodes']));
    }
  };
  dE['prototype']['_handleScriptToken'] = function (e4) {
    var e5 = this, e6 = this['parser']['clear']();
    e6 && this['writeQueue']['unshift'](e6);
    e4['src'] = e4['attrs']['src'] || e4['attrs']['SRC'];
    if (e4 = this['options']['beforeWriteToken'](e4)) e4['src'] && this['scriptStack']['length'] ? this['deferredRemote'] = e4 : this['_onScriptStart'](e4), this['_writeScriptToken'](e4, function () {
      e5['_onScriptDone'](e4);
    });
  };
  dE['prototype']['_handleStyleToken'] = function (e7) {
    var e8 = this['parser']['clear']();
    e8 && this['writeQueue']['unshift'](e8);
    e7['type'] = e7['attrs']['type'] || e7['attrs']['TYPE'] || 'text/css';
    (e7 = this['options']['beforeWriteToken'](e7)) && this['_writeStyleToken'](e7);
    e8 && this['write']();
  };
  dE['prototype']['_writeStyleToken'] = function (e9) {
    var ea = this['_buildStyle'](e9);
    this['_insertCursor'](ea, 'ps-style');
    e9['content'] && (ea['styleSheet'] && !ea['sheet'] ? ea['styleSheet']['cssText'] = e9['content'] : ea['appendChild'](this['doc']['createTextNode'](e9['content'])));
  };
  dE['prototype']['_buildStyle'] = function (eb) {
    var ec = this['doc']['createElement'](eb['tagName']);
    ec['setAttribute']('type', eb['type']);
    ap(eb['attrs'], function (eb, ee) {
      ec['setAttribute'](eb, ee);
    });
    return ec;
  };
  dE['prototype']['_insertCursor'] = function (ef, eg) {
    this['_writeImpl']('<span\x20id=\x22' + eg + '\x22/>');
    (eg = this['doc']['getElementById'](eg)) && eg['parentNode']['replaceChild'](ef, eg);
  };
  dE['prototype']['_onScriptStart'] = function (eh) {
    eh['outerWrites'] = this['writeQueue'];
    this['writeQueue'] = [];
    this['scriptStack']['unshift'](eh);
  };
  dE['prototype']['_onScriptDone'] = function (ei) {
    ei !== this['scriptStack'][0x0] ? this['options']['error']({'msg': 'Bad\x20script\x20nesting\x20or\x20script\x20finished\x20twice'}) : (this['scriptStack']['shift'](), this['write']['apply'](this, ei['outerWrites']), !this['scriptStack']['length'] && this['deferredRemote'] && (this['_onScriptStart'](this['deferredRemote']), this['deferredRemote'] = null));
  };
  dE['prototype']['_writeScriptToken'] = function (ej, ek) {
    var el = this['_buildScript'](ej), em = this['_shouldRelease'](el), bg = this['options']['afterAsync'];
    ej['src'] && (el['src'] = ej['src'], this['_scriptLoadHandler'](el, em ? bg : function () {
      ek();
      bg();
    }));
    try {
      this['_insertCursor'](el, 'ps-script'), el['src'] && !em || ek();
    } catch (eo) {
      this['options']['error'](eo), ek();
    }
  };
  dE['prototype']['_buildScript'] = function (ep) {
    var eq = this['doc']['createElement'](ep['tagName']);
    ap(ep['attrs'], function (ep, es) {
      eq['setAttribute'](ep, es);
    });
    ep['content'] && (eq['text'] = ep['content']);
    return eq;
  };
  dE['prototype']['_scriptLoadHandler'] = function (et, eu) {
    function ev() {
      et = et['onload'] = et['onreadystatechange'] = et['onerror'] = null;
    }

    function ew(et) {
      ev();
      E(et);
      null != eu && eu();
      eu = null;
    }

    function bg(et, eu) {
      var ev = et['on' + eu];
      null != ev && (et['_on' + eu] = ev);
    }

    var E = this['options']['error'];
    bg(et, 'load');
    bg(et, 'error');
    Object['assign'](et, {
      'onload': function () {
        if (et['_onload']) try {
          et['_onload']['apply'](this, Array['prototype']['slice']['call'](arguments, 0x0));
        } catch (eD) {
          ew({'msg': 'onload\x20handler\x20failed\x20' + eD + '\x20@\x20' + et['src']});
        }
        ev();
        null != eu && eu();
        eu = null;
      }, 'onerror': function () {
        if (et['_onerror']) try {
          et['_onerror']['apply'](this, Array['prototype']['slice']['call'](arguments, 0x0));
        } catch (eE) {
          ew({'msg': 'onerror\x20handler\x20failed\x20' + eE + '\x20@\x20' + et['src']});
          return;
        }
        ew({'msg': 'remote\x20script\x20failed\x20' + et['src']});
      }, 'onreadystatechange': function () {
        /^(loaded|complete)$/['test'](et['readyState']) && (ev(), null != eu && eu(), eu = null);
      }
    });
  };
  dE['prototype']['_shouldRelease'] = function (eF) {
    return !/^script$/i['test'](eF['nodeName']) || !!(this['options']['releaseAsync'] && eF['src'] && eF['hasAttribute']('async'));
  };
  var eG = {
    'afterAsync': aO,
    'afterDequeue': aO,
    'afterStreamStart': aO,
    'afterWrite': aO,
    'autoFix': !0x0,
    'beforeEnqueue': aO,
    'beforeWriteToken': function (eK) {
      return eK;
    },
    'beforeWrite': function (eL) {
      return eL;
    },
    'done': aO,
    'error': function (eM) {
      throw Error(eM['msg']);
    },
    'releaseAsync': !0x1
  }, eH = 0x0, eI = [], eJ = null;
  Object['assign'](ba, {'streams': {}, 'queue': eI, 'WriteStream': dE});
  bg('kljfslkj', bf());
  (function (eN, eO, eP) {
    function eQ() {
      if (0x0 === jY['length']) k0 = !0x1; else {
        k0 = !0x0;
        jZ = jY['length'] - 0x1;
        var eN = new XMLHttpRequest(), eO = {'stat': jY, 'referrer': k5, 'url': k7, 'v': '2.2.1-7c14a8a', 'r': jX};
        try {
          eN['open']('POST', k1, !0x0), eN['onreadystatechange'] = function () {
            0x4 == eN['readyState'] && (0xc8 != eN['status'] ? setTimeout(eQ, 0xbb8) : (jY = jY['slice'](jZ + 0x1), 0x0 < jY['length'] ? eQ() : k0 = !0x1));
          }, eN['send'](JSON[bg('ȫȬȪȱȶȿȱȾȡ', bf())](eO));
        } catch (eT) {
          k0 = !0x1, setTimeout(function () {
            eQ();
          }, 0xbb8);
        }
      }
    }

    function E(eN, eO, eX, eY, eP) {
      eN = {'t': eN};
      null !== eO && void 0x0 !== eO && (eN['bId'] = parseInt(eO));
      null !== eX && void 0x0 !== eX && (eN['aId'] = parseInt(eX));
      void 0x0 === eY && (eY = performance['now']());
      eP && (eN['extra'] = eP);
      eN['ts'] = parseInt(eY);
      jY[bg('ȨȭȫȰ', bf())](eN);
      k0 || typeof eQ !== bg('ȾȭȶȻȬȱȷȶ', bf()) || eQ();
    }

    function J(eN, eO, f3, f4, eP) {
      void 0x0 === eO && (eO = '');
      eN['innerHTML'] = '';
      eN['removeAttribute']('data-invisible');
      eN['style']['clear'] = 'both';
      eN['offsetWidth'] || eN['offsetHeight'] || eN['getClientRects']()['length'] ? 'jquery' === f4 ? (c2(eN, eO), dE(eN, f3, eP), bZ(eN) && E('injected', eN['getAttribute']('data-id'), f3)) : ba(eN, eO, {
        'done': function () {
          dE(eN, f3, eP);
          bZ(eN) && E('injected', eN['getAttribute']('data-id'), f3);
        }, 'error': function () {
          E('injected', eN['getAttribute']('data-id'), f3);
          eN['setAttribute']('data-full', 0x1);
        }
      }) : (eN['setAttribute']('data-invisible', '1'), E('invisible', eN['getAttribute']('data-id'), f3), eN['setAttribute']('data-full', 0x1));
    }

    function dE(f7, f8, f9) {
      performance['now']();
      f7['setAttribute']('data-full', 0x1);
      if (!f7['getAttribute']('data-subscribed')) {
        if (f7['childNodes'][0x0] && f7['childNodes'][0x0]['classList'] && f7['childNodes'][0x0]['classList']['contains']('displayBlock')) fa:{
          var fc = f7, fc = void 0x0 === fc ? !0x1 : fc, eP = [];
          if (fc) eP[bg('ȨȭȫȰ', bf())](fc['childNodes'][0x0]); else for (var fc = eO['querySelectorAll']('.content_rb'), eQ = 0x0; eQ < fc['length']; eQ++) for (var fg = 0x0; fg < fc[eQ]['childNodes']['length']; fg++) if (fc[eQ]['childNodes'][fg]['classList'] && fc[eQ]['childNodes'][fg]['classList']['contains']('displayBlock')) eP[bg('ȨȭȫȰ', bf())](fc[eQ]['childNodes'][fg]);
          if (0x0 < eP['length']) for (fc = 0x0; fc < eP['length']; fc++) {
            var eQ = eP[fc], fg = eP[fc]['childNodes'][0x0],
              E = fg['getAttribute']('data-type') ? fg['getAttribute']('data-type') : 'sticky', fk, J, dE, U, dd, eJ,
              bD, a3, fs, aO, W, fv, af, bq, cp, fz = 0x0, cw = 0x0, cA = 0x0, d8;
            f9 || (f9 = fg['getAttribute']('data-config') ? fg['getAttribute']('data-config') : '', f9 = JSON['parse'](f9));
            f9 && f9[E] && (fk = f9[E]['horizontal'] ? f9[E]['horizontal'] : 'simple', J = f9[E]['vertical'] ? f9[E]['vertical'] : '.entry-content', dE = f9[E]['showType'] ? f9[E]['showType'] : 'simple', U = f9[E]['delay'] ? f9[E]['delay'] : 0x0, dd = f9[E]['delayType'] ? f9[E]['delayType'] : 'sec', eJ = f9[E]['close'] ? f9[E]['close'] : 0x0, bD = f9[E]['closeType'] ? f9[E]['closeType'] : 'sec', a3 = f9[E]['closeOnSession'] ? f9[E]['closeOnSession'] : 0x0, fs = f9[E]['darkArea'] ? f9[E]['darkArea'] : 0x0, aO = f9[E]['height'] ? f9[E]['height'] : 0x0, W = f9[E]['width'] ? f9[E]['width'] : 0x0, fz = f9[E]['indent'] ? f9[E]['indent'] : 0x0, cw = f9[E]['indentVertical'] ? f9[E]['indentVertical'] : 0x0, cA = f9[E]['indentHorizontal'] ? f9[E]['indentHorizontal'] : 0x0, fv = f9[E]['reaction'] ? f9[E]['reaction'] : 0x0, af = f9[E]['christDelay'] ? f9[E]['christDelay'] : 0x0, bq = f9[E]['animation'] ? f9[E]['animation'] : 0x0, cp = f9[E]['closeOnClick'] ? f9[E]['closeOnClick'] : 0x0, d8 = f9[E]['transparent'] ? f9[E]['transparent'] : 0x0);
            fk = fk ? fk : fg['getAttribute']('data-horizontal') ? fg['getAttribute']('data-horizontal') : 'simple';
            J = J ? J : fg['getAttribute']('data-vertical') ? fg['getAttribute']('data-vertical') : '.entry-content';
            dE = dE ? dE : fg['getAttribute']('data-show-type') ? fg['getAttribute']('data-show-type') : 'simple';
            U = U ? U : fg['getAttribute']('data-delay') ? fg['getAttribute']('data-delay') : 0x0;
            dd = dd ? dd : fg['getAttribute']('data-delay-type') ? fg['getAttribute']('data-delay-type') : 'sec';
            eJ = eJ ? eJ : fg['getAttribute']('data-close') ? fg['getAttribute']('data-close') : 0x0;
            bD = bD ? bD : fg['getAttribute']('data-close-type') ? fg['getAttribute']('data-close-type') : 'sec';
            a3 = a3 ? a3 : fg['getAttribute']('data-close-on-session') ? fg['getAttribute']('data-close-on-session') : 0x0;
            fs = fs ? fs : fg['getAttribute']('data-darkarea') ? fg['getAttribute']('data-darkarea') : 0x0;
            aO = aO ? aO : fg['getAttribute']('data-height') ? fg['getAttribute']('data-height') : 0x0;
            W = W ? W : fg['getAttribute']('data-width') ? fg['getAttribute']('data-width') : 0x0;
            fv = fv ? fv : fg['getAttribute']('data-reaction') ? fg['getAttribute']('data-reaction') : 0x0;
            af = af ? af : fg['getAttribute']('data-christ-delay') ? fg['getAttribute']('data-christ-delay') : 0x0;
            bq = bq ? bq : fg['getAttribute']('data-animation') ? fg['getAttribute']('data-animation') : 0x0;
            var eU = ke['length'];
            if ('stickyFixed' === E) eQ['style']['position'] = 'relative', fg['style']['position'] = 'absolute', fg['style']['top'] = 0x0, fg['style']['left'] = 0x0, fg['style']['right'] = 0x0, eQ['setAttribute']('data-show', 0x0), fz = {
              'indent': fz ? fz : U,
              'height': aO
            }, ap(fz, eQ, fg, eU); else if ('skyscraper' === E) {
              aO = Number(aO);
              U = Number(U);
              fz = Number(fz);
              0x14 > aO && 0x0 < aO && (aO *= eN['innerHeight']);
              'simple' !== fk && (eQ['style']['position'] = 'absolute');
              E = document['querySelector'](J);
              if (!E) break fa;
              fz = {'horizontal': fk, 'vertical': J, 'width': W, 'minHeight': aO, 'indent': fz ? fz : U};
              cv(fz, eQ, fg, E, eU);
            } else 'overflow' === E ? (eU = document[bg('ȻȪȽȹȬȽȝȴȽȵȽȶȬ', bf())]('div'), eU['className'] = 'rb_overflow_dark', eU['style']['display'] = 'none', eU['style']['position'] = 'fixed', eU['style']['top'] = '0px', eU['style']['left'] = '0px', eU['style']['right'] = '0px', eU['style']['bottom'] = '0px', eU['style']['margin'] = '0px', eU['style']['background'] = 'rgba(0,0,0,0.6)', eQ['appendChild'](eU), eQ['style']['position'] = 'relative', fg['style']['position'] = 'relative', fg['style']['zIndex'] = 0x1, fz = {
              'delay': U,
              'delayType': dd,
              'close': eJ,
              'closeType': bD
            }, aE(fz, eQ, fg)) : (0x0 < cp && eQ['parentNode']['setAttribute']('rb-close-on-click', 0x1), fg['style']['position'] = 'fixed', fg['style']['z-index'] = 0x3b9ac9ff, fg['style']['top'] = 0x0, fg['style']['left'] = 0x0, 0x0 === Number(d8) && (fg['style']['background-color'] = '#fff'), eQ['setAttribute']('data-show', 0x0), fz = {
              'delay': U,
              'delayType': dd,
              'close': eJ,
              'closeType': bD,
              'closeOnSession': a3,
              'reaction': fv,
              'darkArea': fs,
              'christDelay': af,
              'animation': bq,
              'showType': dE,
              'indentVertical': cw,
              'indentHorizontal': cA
            }, bT(fz, eQ, fg, eU));
          }
        }
        k2['adIds'][f8] = f7;
        f7['getAttribute']('data-subscribed', 0x1);
      }
    }

    function U() {
      for (var eN = document['querySelectorAll']('.content_rb[data-id]'), eO = {}, fH = 0x0; fH < eN['length']; ++fH) if (!(fE && Array['isArray'](fE) && 0x0 <= fE['indexOf'](eN[fH]['getAttribute']('data-id')))) {
        var fI = eN[fH]['getAttribute']('data-state');
        fI || (fI = 'new');
        eO[fI] || (eO[fI] = []);
        eO[fI][bg('ȨȭȫȰ', bf())](eN[fH]);
      }
      return eO;
    }

    function dd(eN, eO) {
      for (var fM = eN['map'](function (eN) {
        return eO ? 0x3 : eN['getAttribute']('data-id');
      }), fN = performance['now'](), fO = 0x0; fO < eN['length']; ++fO) eN[fO]['getAttribute']('data-time-fetch') || (E('fetch', fM[fO], null), eN[fO]['setAttribute']('data-time-fetch', fN)), eN[fO]['setAttribute']('data-state', 'fetching');
      var eP = new XMLHttpRequest(), fN = {
        'blocksId': fM,
        'isAdBlock': k2['isAdBlockDetect'],
        'referrer': k5,
        'sessionReferrer': k8,
        'url': k7,
        'v': '2.2.1-~~SHA~VERSION~STRING~~',
        'r': jX,
        'city': bq('rb_city'),
        'region': bq('rb_region'),
        'countryCode': bq('rb_country_code'),
        'width': k6,
        'skip': k9
      };
      try {
        eP['open']('POST', k1, !0x0), eP['onreadystatechange'] = function () {
          if (0x4 == eP['readyState']) if (0xc8 != eP['status']) 'abort' != eP['statusText'] && bN(k3); else {
            var eO = JSON['parse'](eP['responseText']);
            if (!eO['error']) for (var fN = performance['now'](), fO = 0x0; fO < eN['length']; ++fO) eO['blocks'][fO] && (d8(fM[fO], eN[fO], eO['blocks'][fO]), eN[fO]['setAttribute']('data-time-fetched', fN));
          }
        }, eP['send'](JSON[bg('ȫȬȪȱȶȿȱȾȡ', bf())](fN));
      } catch (fV) {
        for (bN(k3), fO = 0x0; fO < eN['length']; ++fO) 'fetching' === eN[fO]['getAttribute']('data-state') && eN[fO]['setAttribute']('data-state', 'new');
      }
    }

    function eJ(eN) {
      for (var eO = [], fZ = 0x0; fZ < eN['length']; ++fZ) {
        var g0;
        g0 = eN[fZ];
        var eP = g0['getAttribute']('data-id');
        kc[eP] ? (d8(eP, g0, kc[eP]), g0['setAttribute']('data-state', 'fetched'), g0['setAttribute']('data-time-fetched', performance['now']()), g0 = !0x0) : g0 = !0x1;
        if (!g0) eO[bg('ȨȭȫȰ', bf())](eN[fZ]);
      }
      0x0 < eO['length'] && dd(eO, !0x1);
    }

    function a3(eN, eO) {
      if ((void 0x0 === eO ? 0x0 : eO) || bZ(eN)) eO = 0x0 === eN['clientHeight'], parseInt(eN['getAttribute']('data-thick')) || eO || (eN['setAttribute']('data-thick', 0x1), E('thick', eN['getAttribute']('data-id'), eN['getAttribute']('data-aid')));
    }

    function aO(eN) {
      for (var eO = performance['now'](), g8 = 0x0; g8 < eN['length']; ++g8) {
        var g9 = eN[g8], eP = g9['getAttribute']('data-time-fetched'), E = 0x0 === g9['clientHeight'],
          eQ = '1' === g9['getAttribute']('data-ad-block'), bg = !!g9['getAttribute']('data-time-reset'),
          bf = 0x3a98 <= g9['getAttribute']('data-refresh-id');
        a3(g9);
        eQ && E && !bg ? eQ && E && 0xbb8 < eO - eP && g9['setAttribute']('data-state', 'reset') : bf ? g9['setAttribute']('data-state', 'refresh-wait') : E || g9['setAttribute']('data-state', 'done');
      }
    }

    function W(eN) {
      for (var eO = performance['now'](), gi = 0x0; gi < eN['length']; ++gi) eN[gi]['setAttribute']('data-time-reset', eO);
      dd(eN, !0x0);
    }

    function af(eN) {
      for (var eO = eP['innerHeight'], gm = performance['now'](), gn = 0x0; gn < eN['length']; ++gn) {
        var E = eN[gn];
        a3(E);
        var eQ = E['getAttribute']('data-time-fetched'), gq = E['getBoundingClientRect'](),
          bg = E['getAttribute']('data-refresh-id'),
          bf = E['childNodes'][0x0] && E['childNodes'][0x0]['classList'] && E['childNodes'][0x0]['classList']['contains']('displayBlock'),
          J = 0x0, dE = 0x0;
        bf && (bf = E['childNodes'][0x0], J = bf['getAttribute']('data-time-show'), dE = bf['getAttribute']('data-show'));
        (bf && '1' === dE && gm - J >= bg || !bf && gm - eQ >= bg && 0x0 < gq['bottom'] && gq['top'] < eO) && E['setAttribute']('data-state', 'new');
      }
    }

    function cw() {
      var eO = U(), gx;
      for (gx in eO) {
        k4 || (k2['isAdBlockDetect'] = bn(), k4 = !0x0);
        var gy = eO[gx];
        'new' === gx ? eJ(gy) : 'fetched' === gx ? aO(gy) : 'reset' === gx ? W(gy) : 'refresh-wait' === gx && af(gy);
      }
      eN['setTimeout'](function () {
        cw();
      }, 0x3e8 > performance['now']() ? 0x32 : 0x1388 < performance['now']() ? 0x12c : 0x96);
    }

    function d8(eN, gB, gC) {
      gB['removeAttribute']('data-full');
      if (gC['ads'] && gC['ads']['id'] && -0x1 !== gC['ads']['id']) {
        gB['setAttribute']('data-aId', gC['ads']['id']);
        gB['setAttribute']('data-order', db++);
        gB['innerHtml'] = '';
        gB['setAttribute']('data-thick', 0x0);
        0xf <= gC['ads']['refresh'] && gB['setAttribute']('data-refresh-id', (0x3e8 * gC['ads']['refresh'])['toString']());
        gC['ads']['type'] && 'skyscraper' === gC['ads']['type'] && (gC['ads']['code'] = gC['ads']['code']['replace'](/(\r\n|\r|\n)/gi, '\x0d\x0a'), eN = gC['ads']['code']['replace'](/^<div class='displayBlock'><div data-type=[^>]*>/gi, '')['replace'](/<\/div><\/div>$/gi, ''), gC['ads']['code'] = gC['ads']['code']['replace'](eN, '<div\x20class=\x22rb_item\x22><div>' + eN['replace'](/[\r\n]{6,}/gi, '</div></div>\x0d\x0a\x0d\x0a<div\x20class=\x22rb_item\x22><div>') + '</div></div>'), gC['ads']['code'] = gC['ads']['code']['replace']('<div\x20class=\x22rb_item\x22><div></div></div>', ''), gC['ads']['code'] = gC['ads']['code']['replace']('<div\x20class=\x27displayBlock\x27>', '<div\x20class=\x27displayBlock\x27\x20style=\x27opacity:\x200;\x27>'));
        gC['ads']['config'] && gC['ads']['config']['base'] && 0x0 < gC['ads']['config']['base']['unique'] && (k9[bg('ȨȭȫȰ', bf())](gC['ads']['id']), bs('rb-unique-ads', JSON[bg('ȫȬȪȱȶȿȱȾȡ', bf())](k9), {
          'path': '/',
          'expires': 0x1e28500
        }));
        gC['ads']['siteAdBlock'] && gC['ads']['blockAdBlock'] && gB['setAttribute']('data-ad-block', 0x1);
        if ('asap' === gC['ads']['loadTime']) J(gB, gC['ads']['code'], gC['ads']['id'], gC['ads']['insertType'], gC['ads']['config']); else if ('onLoad' === gC['ads']['loadTime']) eO[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('DOMContentLoaded', function () {
          J(gB, gC['ads']['code'], gC['ads']['id'], gC['ads']['insertType'], gC['ads']['config']);
        });
        gB['setAttribute']('data-state', 'fetched');
      } else gB['setAttribute']('data-state', 'no-block');
    }

    function eI(eN, eO, gG, eP, eQ) {
      var gJ = performance['now']();
      0xbb8 < gJ - kg && (kg = gJ, eP ? E('click', eO, gG, void 0x0, {
        'teaserId': eP,
        'teaserGroupId': eQ
      }) : E('click', eO, gG), 0x0 < eN['getAttribute']('rb-close-on-click') && setTimeout(function () {
        eN['style']['display'] = 'none';
        eN['setAttribute']('data-finished', 0x0);
        eN['children']['length'] && eN['children'][0x0]['classList']['contains']('displayBlock') && eN['children'][0x0]['setAttribute']('data-close', 0x1);
      }, 0x1f4));
    }

    function cA(eN, eO, gN, eP) {
      for (var gP = [], gQ = 0x3; gQ < arguments['length']; ++gQ) gP[gQ - 0x3] = arguments[gQ];
      var gR = eN['getElementsByTagName']('img');
      if (0x0 < gR['length']) {
        if (void 0x0 === ke[eO]) for (gQ = ke[eO] = 0x0; gQ < gR['length']; ++gQ) 0x0 < gR[gQ]['naturalWidth'] && 0x0 < gR[gQ]['naturalHeight'] ? ke[eO] += 0x1 : gR[gQ]['onload'] = function () {
          ke[eO] += 0x1;
          ke[eO] >= gR['length'] && typeof gN === bg('ȾȭȶȻȬȱȷȶ', bf()) && gN(gP);
        };
        ke[eO] >= gR['length'] && typeof gN === bg('ȾȭȶȻȬȱȷȶ', bf()) && gN(gP);
      } else typeof gN === bg('ȾȭȶȻȬȱȷȶ', bf()) && gN(gP);
    }

    function ap(eO, gU, gV, eP, E) {
      E = void 0x0 === E ? 0x0 : E;
      0x14 > eO['height'] ? 0x0 < gV['clientHeight'] ? cA(gV, eP, aH, eO, gU, gV) : 0xa < E || eN['setTimeout'](function () {
        ap(eO, gU, gV, eP, E + 0x1);
      }, cp(E)) : cu(eO, gU, gV);
    }

    function aH(eN) {
      var eO, h1, h2;
      eN && (eO = eN[0x0], h1 = eN[0x1], h2 = eN[0x2]);
      eO && h1 && h2 && (0x14 > eO['height'] && (eO['displayHeight'] = h2['clientHeight'] * eO['height']), cu(eO, h1, h2));
    }

    function cu(eO, h5, h6) {
      eO['indent'] || (eO['indent'] = 0x0);
      eO['indent'] = Math['max'](0x0, eO['indent']);
      eO['indent'] /= 0x3e8;
      h5['style']['minHeight'] = eO['displayHeight'] + 'px';
      h5['setAttribute']('data-show', 0x1);
      h5['setAttribute']('data-time-show', performance['now']());
      eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('scroll', function () {
        var eN = h6['clientHeight'], h8 = h5['getBoundingClientRect']();
        h6['style']['top'] = Math['min'](Math['max'](eO['indent'] - h8['top'], 0x0), eO['displayHeight'] - eN) + 'px';
      });
    }

    function cv(eO, hb, hc, eP, E, eQ) {
      eQ = void 0x0 === eQ ? 0x0 : eQ;
      var hg = hb['parentNode'];
      'done' == hg['getAttribute']('data-state') || 'fetched' == hg['getAttribute']('data-state') ? cA(hc, E, cG, eO, hb, hc, eP) : 0x5 < eQ || eN['setTimeout'](function () {
        cv(eO, hb, hc, eP, E, eQ + 0x1);
      }, cp(eQ));
    }

    function cG(eN) {
      var eO, hk, hl;
      eO = eN[0x0];
      hk = eN[0x1];
      hl = eN[0x2];
      eN = eN[0x3];
      if (eO && hk && hl && eN) {
        var eP = hl['getElementsByClassName']('rb_item');
        dD(eO, hk, hl, eN, eP, eP['length']);
      }
    }

    function dc(eN, eO, hq) {
      var hr = eN['clientHeight'];
      'simple' === eO && (eN = bz(eN), (hq = bz(hq)) && eN && (hr += eN['top'] - hq['top']));
      return hr;
    }

    function dD(eO, hu, hv, eP, E, eQ, bg) {
      bg = void 0x0 === bg ? 0x0 : bg;
      for (var hA = dc(eP, eO['horizontal'], hu), hB = 0x0, hC = 0x0, bf = 0x0; bf < eQ; bf++) {
        var f0 = E[bf]['clientHeight'];
        if (0x1 > f0 && 0x5 < bg) E[bf]['parentNode']['removeChild'](E[bf]), bf--, eQ--; else {
          if (0x1 > f0) {
            eN['setTimeout'](function () {
              dD(eO, hu, hv, eP, E, eQ, bg + 0x1);
            }, cp(bg));
            return;
          }
          hC >= hA - f0 ? (E[bf]['parentNode']['removeChild'](E[bf]), bf--, eQ--) : (hB++, hC += eO['minHeight'], E[bf]['setAttribute']('data-scroll', 0x1));
        }
      }
      0x0 < hB && cS(eO, hu, hv, eP, E, eQ);
    }

    function cS(eO, hH, hI, eP, E, eQ) {
      'simple' !== eO['horizontal'] && (hI = bz(eP), hH['style']['top'] = hI['top'] + 'px', hH['style']['left'] = '100%');
      hI = dc(eP, eO['horizontal'], hH);
      0x0 >= eO['width'] && (eO['width'] = hH['clientWidth']);
      hH['style']['width'] = eO['width'] + 'px';
      hH['style']['height'] = hI + 'px';
      var hM = 0x0;
      0x0 >= eO['minHeight'] && (hM = hI / eQ);
      for (var hN = 0x0, f0 = 0x0; f0 < eQ; f0++) hM ? E[f0]['style']['height'] = hM + 'px' : f0 >= eQ - 0x1 ? E[f0]['style']['height'] = hI - hN + 'px' : (E[f0]['style']['height'] = eO['minHeight'] + 'px', hN += eO['minHeight']);
      hI = hH['getBoundingClientRect']();
      'right' === eO['horizontal'] ? (hH['style']['left'] = 'auto', hH['style']['right'] = '0') : 'left' === eO['horizontal'] ? (hH['style']['left'] = '0', hH['style']['right'] = 'auto') : 'scrollRight' === eO['horizontal'] ? hI['top'] <= eO['indent'] ? (hH['style']['left'] = 'auto', hH['style']['right'] = 0x0) : (hH['style']['left'] = 'auto', hH['style']['right'] = -eO['width'] + 'px') : 'scrollLeft' === eO['horizontal'] && (hI['top'] <= eO['indent'] ? (hH['style']['right'] = 'auto', hH['style']['left'] = 0x0) : (hH['style']['right'] = 'auto', hH['style']['left'] = -eO['width'] + 'px'));
      ah(eO, hH, E, eQ, eP);
      eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('scroll', function () {
        ah(eO, hH, E, eQ, eP);
      });
      hH['style']['opacity'] = 0x1;
      hH['setAttribute']('data-show', 0x1);
      hH['setAttribute']('data-time-show', performance['now']());
    }

    function ah(eN, hR, hS, eP, E) {
      E = dc(E, eN['horizontal'], hR);
      var hV = 0x0;
      hR['style']['height'] = E + 'px';
      0x0 >= eN['minHeight'] && (hV = E / eP);
      eP = hR['getBoundingClientRect']();
      'scrollRight' === eN['horizontal'] ? eP['top'] <= eN['indent'] && eP['left'] >= eO['body']['clientWidth'] ? bD(hR, 'right', 0x0, 0x12c) : eP['top'] > eN['indent'] && eP['right'] <= eO['body']['clientWidth'] && bD(hR, 'right', -eN['width'], 0x12c) : 'scrollLeft' === eN['horizontal'] && (eP['top'] <= eN['indent'] && 0x0 >= eP['right'] ? bD(hR, 'left', 0x0, 0x12c) : eP['top'] > eN['indent'] && 0x0 <= eP['left'] && bD(hR, 'left', -eN['width'], 0x12c));
      if (hS && 0x0 < hS['length']) for (var eQ, hX = 0x0, bg = 0x0; bg < hS['length']; bg++) {
        if (hV) hS[bg]['style']['height'] = hV + 'px'; else if (hX >= E) {
          hS[bg]['parentNode']['removeChild'](hS[bg]);
          bg--;
          continue;
        } else bg >= hS['length'] - 0x1 ? hS[bg]['style']['height'] = E - hX + 'px' : (hS[bg]['style']['height'] = Math['min'](eN['minHeight'], E - hX) + 'px', hX += eN['minHeight']);
        eP = hS[bg]['childNodes'][0x0];
        hR = hS[bg]['getBoundingClientRect']();
        eQ = eP['getBoundingClientRect']();
        hS[bg]['style']['position'] = 'relative';
        hR['bottom'] <= eQ['height'] + eN['indent'] ? (eP['style']['position'] = 'absolute', eP['style']['bottom'] = 0x0, eP['style']['top'] = 'auto') : (hR['top'] >= eN['indent'] ? (eP['style']['position'] = 'absolute', eP['style']['top'] = 0x0) : (eP['style']['position'] = 'fixed', eP['style']['width'] = eN['width'] + 'px', eP['style']['top'] = eN['indent'] + 'px'), eP['style']['bottom'] = 'auto');
      }
    }

    function aE(eO, i1, i2, eP) {
      eP = void 0x0 === eP ? 0x0 : eP;
      'done' == i1['parentNode']['getAttribute']('data-state') || 'fetched' == i1['parentNode']['getAttribute']('data-state') ? aK(eO, i1, i2) : 0xa < eP || eN['setTimeout'](function () {
        aE(eO, i1, i2, eP + 0x1);
      }, cp(eP));
    }

    function aK(eO, i6, i7, eP) {
      0x0 < i7['clientHeight'] && 0x0 < i7['clientWidth'] ? aP(eO, i6, i7) : 0xa < eP || eN['setTimeout'](function () {
        aK(eO, i6, i7, eP + 0x1);
      }, cp(eP));
    }

    function aP(eO, eP, ic) {
      var id = eP['querySelector']('.rb_overflow_dark');
      if (0x0 < id['length']) {
        var ie = eN['innerHeight'];
        eH(eO, ie, ic, eP, id);
        eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('scroll', function () {
          eH(eO, ie, ic, eP, id);
        });
      }
    }

    function eH(eN, eO, ij, eP, E) {
      ij = ij['getBoundingClientRect']();
      ('px' === eN['delayType'] && ij['top'] >= eN['delay'] || 'percent' === eN['delayType'] && ij['top'] / eO >= eN['delay'] / 0x64) && ('px' === eN['closeType'] && ij['bottom'] <= eO - eN['close'] || 'percent' === eN['closeType'] && ij['bottom'] / eO <= 0x1 - eN['close'] / 0x64) ? (E['style']['display'] = 'block', eP['style']['zIndex'] = 0xf4240, eP['setAttribute']('data-show', 0x1), eP['setAttribute']('data-time-show', performance['now']())) : (E['style']['display'] = 'none', eP['style']['zIndex'] = 'auto', eP['setAttribute']('data-show', 0x0));
    }

    function bT(eO, eP, iq, E, eQ) {
      eQ = void 0x0 === eQ ? 0x0 : eQ;
      0xa < eQ ? cs('Not\x20sticky\x20ad:\x20' + iq['getAttribute']('id')) : 0x0 < iq['clientWidth'] && 0x0 < iq['clientHeight'] ? cB(eO, eP, iq, E) : eN['setTimeout'](function () {
        bT(eO, eP, iq, E, eQ + 0x1);
      }, cp(eQ));
    }

    function cB(eN, eO, iw, eP) {
      var iy = iw['getElementsByTagName']('p'), iz, E;
      if (0x0 < iy['length']) for (iz = 0x0; iz < iy['length']; iz++) E = iy[iz]['innerHTML'], E = E['replace'](/\[[\w\r\n\t\s]+\]/g, ''), E = E['replace']('&nbsp;', ''), E['trim']() || iy[iz]['parentNode']['removeChild'](iy[iz]);
      cA(iw, eP, cE, eN, eO, iw);
    }

    function cE(eN) {
      var eO, iE, eP;
      eO = eN[0x0];
      iE = eN[0x1];
      eP = eN[0x2];
      if (eO && iE && eP) {
        eO['delay'] || (eO['delay'] = 0x0);
        eO['close'] || (eO['close'] = 0x0);
        eO['delayType'] || (eO['delayType'] = 'sec');
        eO['closeType'] || (eO['closeType'] = 'sec');
        eO['christDelay'] = eO['christDelay'] ? 0x3e8 * eO['christDelay'] : 0x0;
        eO['delay'] = Math['max'](0x0, eO['delay']);
        eP['style']['height'] = eP['clientHeight'] + 'px';
        eP['style']['width'] = eP['clientWidth'] + 'px';
        iE['style']['display'] = 'none';
        iE['style']['opacity'] = 0x1;
        var iG = iE['parentNode'], eQ = iG['getAttribute']('data-finished');
        if ('comebacker' === eO['showType']) {
          var iI = 0x0;
          document['getElementsByTagName']('HTML')[0x0][bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('mouseleave', function () {
            0x1 > iI && 0x1 != iE['getAttribute']('data-close') && 0x1 != iE['getAttribute']('data-show') && (E('injected', iG['getAttribute']('data-id'), iG['getAttribute']('data-aid')), cZ(eO, iE, eP, iG, eQ));
            iI++;
          });
        } else cZ(eO, iE, eP, iG, eQ);
      }
    }

    function cZ(eN, eO, iM, eP, E) {
      0x0 < E ? ja(eO, eP, eN) : d5(eO, eP, eN, !0x1);
      0x0 < eN['reaction'] && !E && (E = Object['assign']({}, eN), E['delay'] = eN['close'], E['delayType'] = eN['closeType'], E['close'] = eN['delay'], E['closeType'] = eN['delayType'], d5(eO, eP, E, !0x0));
      0x0 < eN['darkArea'] && (eN = document[bg('ȻȪȽȹȬȽȝȴȽȵȽȶȬ', bf())]('div'), eN['className'] = 'rb_overflow_dark', eN['style']['position'] = 'fixed', eN['style']['top'] = '0px', eN['style']['left'] = '0px', eN['style']['right'] = '0px', eN['style']['bottom'] = '0px', eN['style']['background'] = 'rgba(0,0,0,0.8)', eN['style']['zIndex'] = '100', eO['appendChild'](eN), iM['style']['zIndex'] = '101');
      eO = iM['getBoundingClientRect']();
      eO['right'] > k6 && (iM['childNodes'][0x0]['style']['right'] = eO['right'] - k6 + 'px');
    }

    function d5(eP, E, iS, eQ) {
      var iU;
      if ('sec' === iS['delayType'] && !eQ) eN['setTimeout'](function () {
        ja(eP, E, iS, eQ);
      }, iS['delay']); else if ('sec' === iS['delayType'] && eQ && ('px' === iS['closeType'] || 'percent' === iS['closeType'])) iU = eN['pageYOffset'] || eO['documentElement']['scrollTop'], iV(iU, iS['close'], iS['closeType'], eQ, 0x1) && 0x1 == eP['getAttribute']('data-show') && 0x1 != eP['getAttribute']('data-close') && jw(eP, E), eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('scroll', function () {
        iU = eN['pageYOffset'] || eO['documentElement']['scrollTop'];
        iV(iU, iS['close'], iS['closeType'], eQ, 0x1) && 0x1 == eP['getAttribute']('data-show') && 0x1 != eP['getAttribute']('data-close') && jw(eP, E);
      }); else if ('px' === iS['delayType'] || 'percent' === iS['delayType']) jm(iS, eP, E, !0x1, eQ, 0x0), eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('scroll', function () {
        jm(iS, eP, E, !0x0, eQ, 0x0);
      });
    }

    function iV(eN, eP, iY, E, eQ) {
      E = !0x1;
      if (0x0 === eP || 'sec' === iY) E = !0x0;
      eQ ? 'px' === iY ? E = eN < eP / 0x3e8 : 'percent' === iY && (iY = eO['children'][0x0]['scrollHeight'], E = eN / iY * 0x64 < eP / 0x3e8) : 'px' === iY ? E = eN >= eP / 0x3e8 : 'percent' === iY && (iY = eO['children'][0x0]['scrollHeight'], E = eN / iY * 0x64 >= eP / 0x3e8);
      return E;
    }

    function da(eN, eO, j4) {
      if (eN) {
        var eP = Number(eN['style']['height']['trim']()['replace'](/[^\d\-]/g, '')),
          E = Number(eN['style']['width']['trim']()['replace'](/[^\d\-]/g, ''));
        j4 = j4['getAttribute']('data-finished');
        if (!j4) if ('fade' === eO['animation']) eN['style']['opacity'] = 0x0, bD(eN, 'opacity', 0x1, 0x12c); else if ('centrifuge' === eO['animation']) {
          eN['style']['transform'] = 'rotate(0deg)\x20scale(0)';
          var eQ = 0x0, j8 = 0x0;
          if (0x0 < E || 0x0 < eP) var j9 = setInterval(function () {
            if (0x1 <= eQ && 0x2d0 <= j8) return clearInterval(j9), !0x0;
            0x2d0 > j8 && (j8 += 0x18);
            0x1 > eQ && (eQ += 0x1 / 0x1e);
            eN['style']['transform'] = 'rotate(' + j8 + 'deg)\x20scale(' + eQ + ')';
          }, 0x14);
        }
        eN['classList']['contains']('bottom') ? (eN['style']['top'] = 'auto', 'scrollTop' !== eO['animation'] || j4 ? 'scrollBottom' !== eO['animation'] || j4 ? eN['style']['bottom'] = eO['indentVertical'] + 'px' : (eN['style']['bottom'] = '-' + eP + 'px', bD(eN, 'bottom', eO['indentVertical'], 0x12c)) : (eN['style']['bottom'] = '100%', bD(eN, 'bottom', eO['indentVertical'], 0x12c))) : eN['classList']['contains']('top') ? (eN['style']['bottom'] = 'auto', 'scrollBottom' !== eO['animation'] || j4 ? 'scrollTop' !== eO['animation'] || j4 ? eN['style']['top'] = eO['indentVertical'] + 'px' : (eN['style']['top'] = '-' + eP + 'px', bD(eN, 'top', eO['indentVertical'], 0x12c)) : (eN['style']['top'] = '100%', bD(eN, 'top', eO['indentVertical'], 0x12c))) : eN['classList']['contains']('centerV') && (eN['style']['bottom'] = 'auto', eN['style'][bg('ȵȹȪȿȱȶɵȬȷȨ', bf())] = '-' + eP / 0x2 + 'px', 'scrollBottom' !== eO['animation'] || j4 ? 'scrollTop' !== eO['animation'] || j4 ? eN['style']['top'] = '50%' : (eN['style']['top'] = '-50%', bD(eN, 'top', 0x32, 0x12c)) : (eN['style']['top'] = '150%', bD(eN, 'top', 0x32, 0x12c)));
        eN['classList']['contains']('left') ? (eN['style']['right'] = 'auto', 'scrollRight' !== eO['animation'] || j4 ? 'scrollLeft' !== eO['animation'] || j4 ? eN['style']['left'] = eO['indentHorizontal'] + 'px' : (eN['style']['left'] = '-' + E + 'px', bD(eN, 'left', eO['indentHorizontal'], 0x12c)) : (eN['style']['left'] = '100%', bD(eN, 'left', eO['indentHorizontal'], 0x12c))) : eN['classList']['contains']('right') ? (eN['style']['left'] = 'auto', 'scrollLeft' !== eO['animation'] || j4 ? 'scrollRight' !== eO['animation'] || j4 ? eN['style']['right'] = eO['indentHorizontal'] + 'px' : (eN['style']['right'] = '-' + E + 'px', bD(eN, 'right', eO['indentHorizontal'], 0x12c)) : (eN['style']['right'] = '100%', bD(eN, 'right', eO['indentHorizontal'], 0x12c))) : eN['classList']['contains']('centerH') && (eN['style']['right'] = 'auto', eN['style'][bg('ȵȹȪȿȱȶɵȴȽȾȬ', bf())] = '-' + E / 0x2 + 'px', 'scrollRight' !== eO['animation'] || j4 ? 'scrollLeft' !== eO['animation'] || j4 ? eN['style']['left'] = '50%' : (eN['style']['left'] = '-50%', bD(eN, 'left', 0x32, 0x12c)) : (eN['style']['left'] = '150%', bD(eN, 'left', 0x32, 0x12c)));
      }
    }

    function ja(eO, eP, jd, E) {
      var eQ = performance['now']();
      eO['setAttribute']('data-show', 0x1);
      eO['setAttribute']('data-time-show', eQ);
      eO['childNodes'][0x0] && (eO['childNodes'][0x0]['style']['left'] = 'auto', eO['childNodes'][0x0]['style']['right'] = '100%');
      eO['style']['display'] = 'block';
      'comebacker' === jd['showType'] && (eP['style']['height'] = '1px', a3(eP, !0x0), eP['style']['height'] = 'auto');
      da(eO['childNodes'][0x0], jd, eP);
      if (0x0 < jd['christDelay'] && eO['childNodes'][0x0] && eO['childNodes'][0x0]['childNodes'][0x0]) {
        var jg = eO['childNodes'][0x0]['childNodes'][0x0];
        jg['style']['display'] = 'none';
        jg['style']['opacity'] = 0x0;
        setTimeout(function () {
          jg['style']['display'] = 'table';
          var eN = setInterval(function () {
            0x1 <= jg['style']['opacity'] ? clearInterval(eN) : jg['style']['opacity'] = Number(jg['style']['opacity']) + 0.05;
          }, 0xf);
        }, jd['christDelay']);
      }
      if ((!E || 0x1 != eP['getAttribute']('data-finished')) && 0x1 != eO['getAttribute']('data-close') && 0x0 < jd['close']) if ('sec' === jd['closeType'] && !kb[eP['getAttribute']('data-order')]) kb[eP['getAttribute']('data-order')] = eN['setTimeout'](function () {
        jw(eO, eP);
      }, jd['close']); else if ('px' === jd['closeType'] || 'percent' === jd['closeType']) jm(jd, eO, eP, !0x1, E), eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('scroll', function () {
        jm(jd, eO, eP, !0x0, E);
      });
      eQ = eO['querySelectorAll']('.display-close');
      if (0x0 < eQ['length']) for (var ji = 0x0; ji < eQ['length']; ji++) eQ[ji][bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())](bg('ȻȴȱȻȳ', bf()), function () {
        var eN = this['closest']('.displayBlock');
        eN['setAttribute']('data-show', 0x0);
        eN['setAttribute']('data-close', 0x1);
        eN['style']['display'] = 'none';
        if (jd['closeOnSession'] && 0x0 < jd['closeOnSession']) {
          var eN = bq('rb-sess-close-dis'), eO = [];
          if (eN && (eO = JSON['parse'](eN), 0x0 <= eO['indexOf'](eP['getAttribute']('data-id')))) return !0x0;
          eO[bg('ȨȭȫȰ', bf())](eP['getAttribute']('data-id'));
          bs('rb-sess-close-dis', JSON[bg('ȫȬȪȱȶȿȱȾȡ', bf())](eO), {'path': '/', 'expires': 0x278d00});
        }
      });
      eP['setAttribute']('data-finished', 0x1);
    }

    function jm(eP, E, jp, eQ, bg, bf) {
      bf = void 0x0 === bf ? 0x1 : bf;
      var jt = eN['pageYOffset'] || eO['documentElement']['scrollTop'],
        ju = eO['children'][0x0]['scrollHeight'] - eN['innerHeight'] - jt, jv;
      bg && (jv = ju, ju = jt, jt = jv);
      bf && ('sec' !== eP['delayType'] && iV(jt, eP['delay'], eP['delayType'], bg, bf) || iV(ju, eP['close'], eP['closeType'], bg, bf) && (!eQ || 0x1 != E['getAttribute']('data-close') && 0x1 == E['getAttribute']('data-show'))) ? jw(E, jp) : 'sec' === eP['delayType'] || !iV(jt, eP['delay'], eP['delayType'], bg, bf) || !iV(ju, eP['close'], eP['closeType'], bg, bf) || 0x1 == E['getAttribute']('data-close') || eQ && 0x0 != E['getAttribute']('data-show') || !bg && 'sec' === eP['closeType'] && 0x0 < eP['close'] && kb[jp['getAttribute']('data-order')] || ja(E, jp, eP, bg);
    }

    function jw(eN, eO) {
      var eP = eO['children'];
      0x0 < eP['length'] && (eN = eP[0x0]);
      eN['setAttribute']('data-show', 0x0);
      eN['style']['display'] = 'none';
      eO['setAttribute']('data-finished', 0x0);
      0x0 == eN['children'][0x0]['getAttribute']('data-reaction') && eN['setAttribute']('data-close', 0x1);
    }

    function jA(eN) {
      for (var eP, jD = eO['body']['querySelectorAll']('.content_rb'), E = 0x0; E < jD['length']; E++) if (eN['target'] === jD[E] || jD[E]['contains'](eN['target'])) {
        eP = jD[E];
        break;
      }
      return eP;
    }

    function jF(eN) {
      if (!kh) {
        var eP = jA(eN);
        if (eP) {
          ki = !0x0;
          var jI = null;
          if (eP['children'][0x0] && eP['children'][0x0]['classList'] && eP['children'][0x0]['classList']['contains']('displayBlock') && eP['children'][0x0]['children'][0x0]) {
            var E = 0x0, eQ, bg, jM, bf;
            eQ = eP['children'][0x0]['classList']['contains']('sticky') ? eP['children'][0x0]['children'][0x0]['children'][0x1]['children'] : eP['children'][0x0]['children'][0x0]['children'];
            for (jM = 0x0; jM < eQ['length']; jM++) {
              var J = eQ[jM];
              if (J['classList']['contains']('rb_item')) for (bg = eQ[jM]['children'], bf = 0x0; bf < bg['length'] && !(J = bg[bf], (eN['target'] === J || J['contains'](eN['target'])) && E++, 0x0 < E); bf++) ; else (eN['target'] === J || J['contains'](eN['target'])) && E++;
              if (0x0 < E) break;
            }
            E || (ki = !0x1);
          }
          (eN = eN['target']['closest']('a.table-cell[data-id]')) && (jI = eN['getAttribute']('data-id'));
          ki && (eP['getAttribute']('id'), eN = eP['getAttribute']('data-id'), E = eP['getAttribute']('data-aid'), eI(eP, eN, E, jI), eO['getElementsByTagName']('html')[0x0]['focus'](), ki = !0x1);
        }
      }
    }

    function jP(eN) {
      eN = void 0x0 === eN ? 0x0 : eN;
      if (null == eO['body']) setTimeout(function () {
        jP(eN + 0x1);
      }, cp(eN)); else {
        var eP = eO['body'];
        eP[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('touchend', function (eN) {
          jF(eN);
        });
        eP[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())](bg('ȻȴȱȻȳ', bf()), function (eN) {
          jF(eN);
        });
        eP[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('touchmove', function () {
          kh = !0x0;
        });
        eP[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('touchstart', function () {
          kh = !0x1;
        });
        eP[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('mouseover', function (eN) {
          if (eN = jA(eN)) eN['getAttribute']('id'), k2['isOverIframe'][eN['getAttribute']('data-aid')] = eN;
        });
        eP[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('mouseout', function (eN) {
          if (eN = jA(eN)) eN['getAttribute']('id'), k2['isOverIframe'][eN['getAttribute']('data-aid')] = !0x1;
        });
        bn() && E('adblock', null, null);
      }
    }

    if (!window['rb-rotor-st-fir'] && (window['rb-rotor-st-fir'] = 0x1, 0x1 !== eN['checkIfRBScriptIsLoaded'])) {
      var jW = eP['rbConfig'] || {};
      eN['checkIfRBScriptIsLoaded'] = 0x1;
      var jX = Math['round'](0x39aa400 * Math['random']())['toString'](0x24) + Math['round'](0x39aa400 * Math['random']())['toString'](0x24),
        jY = [], jZ = 0x0, k0 = !0x1,
        k1 = '//' + (jW['rbDomain'] || "park.qmsdxo.ru") + '/' + (jW['rotator'] || 'rotator') + '.json';
      E('start', null, null, jW['start'] || 0x0);
      E('loaded', null, null);
      var k2 = {
          'blocksSent': [],
          'blocksResent': [],
          'adIds': [],
          'refreshId': [],
          'isAdBlockDetect': !0x1,
          'isOverIframe': []
        }, k3 = !0x1, k4 = !0x1, k5 = decodeURI(eO['referrer']),
        k6 = window['innerWidth'] || document['documentElement']['clientWidth'] || document['body']['clientWidth'],
        k7 = decodeURI(location['href']), k8 = '', k9, fE, kb = [];
      '' == k5['trim']() && (k5 = 'http://no.domain/');
      bP(k5);
      k8 = bq(bg('ȪȺɵȫȽȫȫɵȪȽȾ', bf()));
      fE = (fE = bq('rb-sess-close-dis')) ? JSON['parse'](fE) : [];
      !k9 && localStorage && (k9 = localStorage['getItem']('rb-unique-ads')) && (bs('rb-unique-ads', k9, {
        'path': '/',
        'expires': 0x1e28500
      }), localStorage['removeItem']('rb-unique-ads'));
      k9 || (k9 = bq('rb-unique-ads'));
      k9 = k9 ? JSON['parse'](k9) : [];
      var kc = {}, db = 0x0, ke = [];
      eP['getAd'] = function (eN) {
      };
      eN['focus']();
      var kg = 0x0, kh = !0x1, ki = !0x0;
      cw();
      eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('blur', function () {
        !0x0 !== kh && ki && k2['isOverIframe']['forEach'](function (eN, eP) {
          eN && (eI(eN, eN['getAttribute']('data-id'), eP), eO['getElementsByTagName']('html')[0x0]['focus']());
        });
      });
      eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('focus', function () {
      });
      eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('touchend', function (eP) {
        eP['target'] !== eO && eN['blur']();
      });
      setTimeout(jP, cp());
      eN[bg('ȹȼȼȝȮȽȶȬȔȱȫȬȽȶȽȪ', bf())]('load', function () {
        performance['now']();
        E('dom');
        k3 = !0x0;
      });
    }
  }(window, document, window));
}());