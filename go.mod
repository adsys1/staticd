module gitlab.com/adsys1/staticd

go 1.13

require (
	github.com/json-iterator/go v1.1.9
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/klauspost/compress v1.10.1 // indirect
	github.com/oklog/ulid v1.3.1
	github.com/partyzanex/stupid-random v1.0.1
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.18.0
	github.com/valyala/fasthttp v1.9.0
	gitlab.com/adsys1/stat/clickhouse.git v0.0.2
)
