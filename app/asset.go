package app

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"os"
	"time"

	"github.com/pkg/errors"
)

type Header struct {
	Name  []byte
	Value []byte
}

type Asset struct {
	Path string

	Length       int
	LengthGzip   int
	ETag         []byte
	LastModified []byte
	ContentType  []byte

	Data    []byte
	Gzipped []byte
}

func CreateAsset(path string) (Asset, error) {
	file, err := os.Stat(path)
	if err != nil {
		return Asset{}, errors.Wrap(err, "get file info failed")
	}

	data, err := ioutil.ReadFile(path)
	if err != nil {
		return Asset{}, errors.Wrap(err, "read file failed")
	}

	buf := &bytes.Buffer{}
	writer := gzip.NewWriter(buf)

	_, err = writer.Write(data)
	if err != nil {
		return Asset{}, errors.Wrap(err, "write to gzip failed")
	}

	err = writer.Flush()
	if err != nil {
		return Asset{}, errors.Wrap(err, "flush writer failed")
	}

	err = writer.Close()
	if err != nil {
		return Asset{}, errors.Wrap(err, "close writer failed")
	}

	gzipped := buf.Bytes()
	hash := md5.Sum(data)
	eTag := hex.EncodeToString(hash[:])

	asset := Asset{
		Length:       len(data),
		LengthGzip:   len(gzipped),
		Path:         file.Name(),
		Data:         data,
		Gzipped:      gzipped,
		ETag:         []byte(eTag),
		LastModified: []byte(file.ModTime().Format(time.RFC1123)),
		ContentType:  []byte(GetContentType(file.Name())),
	}

	return asset, nil
}
