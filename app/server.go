package app

import (
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"os"
	"os/signal"
	"syscall"
)

func Run() {
	config, err := ReadConfig("")
	if err != nil {
		log.Fatal().Err(err).Msg("read config failed")
	}

	assets, err := config.Assets()
	if err != nil {
		log.Fatal().Err(err).Msg("get assets failed")
	}

	cancel := make(chan os.Signal, 2)

	signal.Notify(cancel, os.Interrupt, syscall.SIGTERM)

	addr := config.Address()
	handler := New(config, assets...)

	go func() {
		log.Info().Msgf("listen on %s", addr)

		if err := fasthttp.ListenAndServe(addr, handler.Handle); err != nil {
			log.Fatal().Err(err)
		}
	}()

	go func() {
		addr := config.MgrAddress()
		mgr := &Manager{
			AssetsPath: config.AssetsPath,
			Handler:    handler,
		}

		log.Info().Msgf("listen on %s", addr)

		if err := fasthttp.ListenAndServe(addr, mgr.Handle); err != nil {
			log.Fatal().Err(err)
		}
	}()

	<-cancel
	os.Exit(0)
}
