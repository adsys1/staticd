package app

import (
	"net/http"
	"os"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
)

type Manager struct {
	AccessToken string
	AssetsPath  string
	Handler     *Handler
}

func (mgr *Manager) Handle(ctx *fasthttp.RequestCtx) {
	if string(ctx.Method()) != http.MethodPost {
		return
	}

	token := string(ctx.Request.Header.Peek("X-Auth-Token"))

	if mgr.AccessToken != token {
		ctx.SetStatusCode(http.StatusForbidden)

		return
	}

	path := string(ctx.Path()[1:])
	assetPath := filepath.Join(mgr.AssetsPath, path)

	file, err := os.OpenFile(assetPath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0664)
	if err != nil {
		log.Fatal().Err(err).Msg("open asset path failed")

		return
	}

	defer func() {
		_ = file.Close()
	}()

	_, err = file.Write(ctx.PostBody())
	if err != nil {
		log.Fatal().Err(err).Msg("write to file failed")

		return
	}

	asset, err := CreateAsset(assetPath)
	if err != nil {
		log.Fatal().Err(err).Msg("create asset failed")

		return
	}

	mgr.Handler.AddAsset(asset)
	ctx.SetStatusCode(http.StatusAccepted)
}
