package app

import (
	"github.com/rs/zerolog/log"
	"github.com/valyala/fasthttp"
	"time"
)

type logger func(ctx *fasthttp.RequestCtx)

func zeroLog(ctx *fasthttp.RequestCtx) {
	since := time.Since(ctx.Time())
	log.Info().
		Bytes("path", ctx.Path()).
		Dur("duration", since).
		Send()
}

func emptyLog(_ *fasthttp.RequestCtx) {
}
