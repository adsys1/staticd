package app

import (
	"bytes"
	"compress/gzip"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
)

type Config struct {
	// http config
	Host string `envconfig:"HOST" default:"" json:"host"`
	Port uint16 `envconfig:"PORT" default:"8092" json:"port"`

	// management http config
	MgrHost  string `envconfig:"MGR_HOST" default:"" json:"host"`
	MgrPort  uint16 `envconfig:"MGR_PORT" default:"8090" json:"port"`
	MgrToken string `envconfig:"MGR_TOKEN" default:"" json:"mgr_token"`

	// assets list
	AssetsPath string `envconfig:"ASSETS_PATH" default:"./assets" json:"assets_path"`
	// identity
	TargetName   string `envconfig:"TARGET_NAME" default:"ad_user_id" json:"target_name"`
	TargetDomain string `envconfig:"TARGET_DOMAIN" default:"localhost" json:"target_domain"`
	TargetPath   string `envconfig:"TARGET_PATH" default:"/" json:"target_path"`
	CookieMaxAge int    `envconfig:"COOKIE_MAX_AGE" default:"630720000" json:"cookie_max_age"`
	TargetSecure bool   `envconfig:"TARGET_SECURE" default:"false" json:"target_secure"`
	// access token

	// ip:port of controld service
	ControlD UDPs `enconfig:"CONTROLD" default:"127.0.0.1:9005" json:"controld"`
	// ip:port of uids service(s)
	UIDs UDPs `enconfig:"UIDS" default:"127.0.0.1:8098" json:"uids"`
}

type UDPs []string

func (udps *UDPs) Decode(value string) error {
	slice := strings.Split(value, ",")
	*udps = slice

	return nil
}

func (config Config) Address() string {
	return fmt.Sprintf("%s:%d", config.Host, config.Port)
}

func (config Config) MgrAddress() string {
	return fmt.Sprintf("%s:%d", config.MgrHost, config.MgrPort)
}

func (config Config) Assets() ([]Asset, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return nil, errors.Wrap(err, "get pwd failed")
	}

	assetsPath := filepath.Join(pwd, config.AssetsPath)

	files, err := ioutil.ReadDir(assetsPath)
	if err != nil {
		return nil, errors.Wrap(err, "read assets dir failed")
	}

	var assets []Asset

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		path := filepath.Join(pwd, config.AssetsPath, file.Name())

		data, err := ioutil.ReadFile(path)
		if err != nil {
			return nil, errors.Wrap(err, "read file failed")
		}

		buf := &bytes.Buffer{}
		writer := gzip.NewWriter(buf)

		_, err = writer.Write(data)
		if err != nil {
			return nil, errors.Wrap(err, "write to gzip failed")
		}

		err = writer.Flush()
		if err != nil {
			return nil, errors.Wrap(err, "flush writer failed")
		}

		err = writer.Close()
		if err != nil {
			return nil, errors.Wrap(err, "close writer failed")
		}

		gzipped := buf.Bytes()
		hash := md5.Sum(data)
		eTag := hex.EncodeToString(hash[:])

		assets = append(assets, Asset{
			Length:       len(data),
			LengthGzip:   len(gzipped),
			Path:         file.Name(),
			Data:         data,
			Gzipped:      gzipped,
			ETag:         []byte(eTag),
			LastModified: []byte(file.ModTime().Format(time.RFC1123)),
			ContentType:  []byte(GetContentType(file.Name())),
		})
	}

	return assets, nil
}

func GetContentType(fileName string) string {
	parts := strings.Split(fileName, ".")
	n := len(parts)

	if n == 1 {
		return ""
	}

	ext := parts[n-1]

	switch ext {
	case "js":
		return "text/javascript; charset=UTF-8"
	case "css":
		return "text/css; charset=utf-8"
	}

	return ""
}

func ReadConfig(prefix string) (*Config, error) {
	config := &Config{}

	err := envconfig.Process(prefix, config)
	if err != nil {
		return nil, errors.Wrap(err, "process environment failed")
	}

	return config, nil
}
